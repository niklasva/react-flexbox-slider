import React, { Component } from 'react';
import styled from 'styled-components';
import Slide from './Slide';

const SliderWrapper = styled.div`
  position: relative;

  /* FIXME: This is to prevent horizontal scrollbar, the slider is actually a bit too large, this should probably be fixed.*/
  overflow-x: hidden;
`;

const ScrollContainer = styled.div`
  width: 100vw;
  display: flex;
  overflow-x: scroll;
  scroll-snap-type: x proximity;
`;

const Slider = styled.div`
  /*width: ${props => { const count = React.Children.count(props.children);
                      return (100*count)/props.numVisible}}vw;*/
  display: flex;
  flex-grow: 0;
  flex-shrink: 1;
  align-items: center;
  justify-content: stretch;
  flex-direction: row;
  flex-wrap: nowrap;
`;

const DirectionButton = styled.div`
  height: 100%;
  width: 4em;
  background-color: rgba(255,255,255, 0.75);
  position: absolute;
  z-index: 100;
  top: 0;
  ${ props => props.direction }: 0;
`;

//TODO: The body gets a weird x-scroll. This could probably be fixed by setting
// overflow-x hidden, but that just seems like an ugly solution.

class FlexSlider extends Component {

  constructor(props) {
    super(props);
    this.state = {
      position: 1000000,
      slides: [],
    };
    
    // Reference to the scroll container
    this.scroll = React.createRef();
    this.animateScroll = this.animateScroll.bind(this);

    this.onSlideMouseIn

    this.handleSlideMouseEnter = this.handleSlideMouseEnter.bind(this);
    this.handleSlideMouseLeave = this.handleSlideMouseLeave.bind(this);
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    // Computer derived state
    let derivedState = {
      // If a slide speed is not provided, set it to visible-1
      slideSpeed: nextProps.slideSpeed ? 
	    nextProps.slideSpeed : Math.floor(nextProps.visible-1) || 1,
      slides: null,
      slideDuration: nextProps.slideDuration ? nextProps.slideDuration : 200,
      hoverSize : nextProps.hoverSize ? nextProps.hoversize : 1.2,
    }

    // concat nextProps.slides to prevState.slides until
    // it has at least minMult*slideSpeed slides
    const minMult = 2.5; 
    // Absolute minimum number of slides
    const minSlides = 4;

    if( nextProps.slides.length > minMult*derivedState.slideSpeed &&
		nextProps.slides.length > minSlides) {
      derivedState.slides = nextProps.slides;
    } else {
      let slides = prevState.slides;
      while(slides.length <= minMult*derivedState.slideSpeed || 
		slides.length <= minSlides) {
        slides = slides.concat(nextProps.slides);
      }
      derivedState.slides = slides;
    }

    return derivedState;
  }

  componentDidMount() {
    // Get the totalt of the scrollable area (scrollWidth) 
    // and the width of the visible area (innerWidth)
    this.scrollWidth = this.scroll.current.scrollWidth;
    this.innerWidth = this.scroll.current.clientWidth;

    // Calculate the width of slides
    this.slideCount = this.state.slides.length;
    this.slideWidth = this.scrollWidth/this.slideCount;

    // Midpoint is different depending on odd or even number of slides
    if(this.props.visible > 2) {
      this.midPoint = Math.floor(this.scrollWidth/2-this.innerWidth/2 
			  - (this.slideCount%2===0 ? 0 : this.slideWidth/2));
    } else {
      this.midPoint = Math.floor(this.scrollWidth/2-this.innerWidth/2
                          - (this.slideCount%2===0 ? this.slideWidth/2 : 0));
    }
    // Center the scroll
    this.scroll.current.scrollLeft = this.midPoint;
    this.scroll.current.addEventListener('scroll', () => this.handleScroll());
  }

  componentWillUnmount() {
    this.scroll.current.removeEventListener('scroll', () => this.handleScroll());
  }

  // This function is called by scroll listener every time scroll position changes.
  // It checks the distance from the "midPoint" (calculated in componentDidMount)
  // and if it's larger than a specified width, it will reorder the slides such that
  // the last slide becomes the first slide, the second becomes the third, and so forth.
  // it then resets the slider to the "midPoint" and adds the remainer between the
  // total distance covered by the reordered slides and the scroll distance to
  // the midPoint. The result is a smooth infinite scroll.
  handleScroll() {
    if(this.animating) {
      // If animating, don't do anything until the animation has finished
      return;
    }
    const scrollDiff = this.scroll.current.scrollLeft - this.midPoint;
    console.log(scrollDiff);

    if(Math.abs(scrollDiff) >= this.slideWidth-20) {
      const posDiff = Math.ceil(scrollDiff/this.slideWidth);
      // The remainder, we have to adjust the scroll position properly using this
      const remainder = scrollDiff - posDiff*this.slideWidth;
      let newPos = 0;
      if(scrollDiff > 0) {
        newPos = this.state.position - posDiff;
      } else {
        newPos = this.state.position - posDiff;
      }

      // Change position so that the order of the elements will shift posDiff places in a ring
      this.setState({
	      position: newPos,
      });

      // Scroll to correct location so it doesn't appear like anything has been done
      this.scroll.current.scrollLeft = this.midPoint + remainder;
      // So that any active animation will adjust its target
      this.animationTarget -= scrollDiff;
      console.log('new position: ' + newPos);
    }
  }

  handleSlideMouseEnter = (event) => {
    //TODO: Add limit on visible area of slide before allowing scroll and hover
    this.animating = true;
    const { scrollLeft } = this.scroll.current;
    const sliderWidth = this.innerWidth;
    const targetLeft = event.target.offsetLeft;
    const slideWidth = this.slideWidth;
    const hoverSlideWidth = slideWidth*this.state.hoverSize;
    // Compensate for buttons, this could also be done by accessing actual value through a ref
    const margin = 64;
    // Calculate the extra widht added by hover.
    // This is needed when hovering on the right of the screen.
    const extraWidth = hoverSlideWidth-slideWidth+15;
    let scrollLeftTransform = 0;
    // Make sure the original slide is in view
    if(targetLeft < (scrollLeft + margin)) {
      scrollLeftTransform -= scrollLeft + margin - targetLeft;
    } else if(targetLeft+slideWidth > scrollLeft + sliderWidth - margin - extraWidth) {
      scrollLeftTransform += targetLeft - ((scrollLeft + sliderWidth - margin - extraWidth)-slideWidth);
    }
    if(Math.abs(scrollLeftTransform) > 50) {
      this.scroll.current.scrollLeft += scrollLeftTransform;
    }
    this.currentScrollTransform = scrollLeftTransform;
    console.log(scrollLeftTransform);
  }

  handleSlideMouseLeave = (event) => {
    this.scroll.current.scrollLeft -= this.currentScrollTransform;
    this.animating = false;
  }

  moveSliderRight = () => {
    //this.scroll.current.scrollLeft = this.midPoint + 
//		  (Math.floor(this.props.visible-1) || 1)*this.slideWidth;
    const newPos = this.midPoint + 
		  this.state.slideSpeed*this.slideWidth;
    this.smoothScroll(newPos, 1);
    // In case the last change is too small for the scroll listener to be triggered,
    // trigger handleScroll manually here.
    this.handleScroll();
  }

  moveSliderLeft = () => {
    //this.scroll.current.scrollLeft = this.midPoint - 
//		  (Math.floor(this.props.visible-1) || 1)*this.slideWidth;
    const newPos = this.midPoint - 
		  this.state.slideSpeed*this.slideWidth
    this.smoothScroll(newPos, -1);
    // In case the last change is too small for the scroll listener to be triggered,
    // trigger handleScroll manually here.
    this.handleScroll();
  }

  smoothScroll = (scrollLeft, direction) => {
    if(this.animating) {
      // wait for the animation to finish before allowing another animation
      return;
    }
    this.animationStart = this.midPoint;
    this.animationTarget = scrollLeft;
    this.animationDirection = direction;
    this.animationDuration = this.state.slideDuration;
    this.animationStartTimestamp = null;
    this.animating = true;
    window.requestAnimationFrame(this.animateScroll);
  }
  
  animateScroll = (timestamp) => {
    if(!this.animationStartTimestamp) {
      this.animationStartTimestamp = timestamp;
    }
    const timePassed = timestamp - this.animationStartTimestamp;
    const scrollLeft = this.scroll.current.scrollLeft;
    const newScrollLeft = this.animationStart + 
		  (timePassed/this.animationDuration)*(this.animationTarget-this.animationStart);
    const targetReached = this.animationDirection*newScrollLeft > 
				  this.animationDirection*this.animationTarget;
    if(!targetReached && timePassed < this.animationDuration) {
      this.scroll.current.scrollLeft = newScrollLeft;
      window.requestAnimationFrame(this.animateScroll);
    } else {
      this.animating = false;
      this.scroll.current.scrollLeft = this.animationTarget;
    }
  }

  render() {
    const { visible } = this.props;
    const { position, slides, hoverSize } = this.state;

    return (
      <SliderWrapper>
	<DirectionButton direction={ 'left' } onClick={ () => this.moveSliderLeft() } />
	<ScrollContainer innerRef={ this.scroll }>
          <Slider numVisible={ visible }>
	      { slides.map((slide, idx) => {
	    	  const order = ((idx+position) % slides.length);
	          return (
	            <Slide key={idx} slide={ slide } order={ order } slideWidth={ 100/visible } mouseEnter={ this.handleSlideMouseEnter } mouseLeave={ this.handleSlideMouseLeave } hoverSize={ hoverSize } sliderWidth={ this.sliderWidth } />
		  )
	        }) }
          </Slider>
        </ScrollContainer>
	<DirectionButton direction={ 'right' }  onClick={ () => this.moveSliderRight() } />
      </SliderWrapper>
    );
  }
}

export default FlexSlider;
